const commitlint = require('danger-plugin-commitlint-gitlab')
const configConventional = require('@commitlint/config-conventional')

;(async function dangerReport() {
  const commitlintConfig = {
    severity: 'fail',
  }
  await commitlint.default(configConventional.rules, commitlintConfig)
})()
