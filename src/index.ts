// Provides dev-time type structures for  `danger` - doesn't affect runtime.
import * as lint from "@commitlint/lint"
import d from "debug"
import { DangerDSLType } from "../node_modules/danger/distribution/dsl/DangerDSL"
const debug = d("danger-plugin:commitlint-gitlab")
declare function message(message: string): void
declare function fail(message: string): void
declare function warn(message: string): void
declare var danger: DangerDSLType

export interface CommitlintPluginConfig {
  severity?: "fail" | "warn" | "message" | "disable"
}

interface Rules {
  "body-leading-blank": Array<number | string>
  "footer-leading-blank": Array<number | string>
  "header-max-length": Array<number | string>
  "scope-case": Array<number | string>
  "subject-case": Array<string[] | number | string>
  "subject-empty": Array<number | string>
  "subject-full-stop": Array<number | string>
  "type-case": Array<number | string>
  "type-empty": Array<number | string>
  "type-enum": Array<string[] | number | string>
}

const defaultConfig = { severity: "fail" }

export default async function check(rules: Rules, userConfig?: CommitlintPluginConfig) {
  const config = { ...defaultConfig, ...userConfig }

  // check MR squash flag and number of commits
  const squashFlag = danger.gitlab.mr.squash
  const commitsArrayLength = danger.gitlab.commits.length

  debug("squash flag: ", squashFlag)
  debug("MR title: ", danger.gitlab.mr.title)

  if (squashFlag === true && commitsArrayLength > 1) {
    // If MR sqash flag is true & multiple commits - title must be correct
    await lintCommitMessage(danger.gitlab.mr.title.replace("Draft:", "").trim(), rules, config.severity)
  } else if (squashFlag === false && commitsArrayLength > 1) {
    // If MR sqash flag is false & multiple commits - at least one commit must be correct
    await evalMessages(danger.gitlab.commits, rules, config.severity)
    // await  messageLint(danger.gitlab.commits, rules, config.severity)
  } else {
    await lintCommitMessage(danger.gitlab.commits[0].message, rules, config.severity)
  }
}

async function lintCommitMessage(commitMessage, rules, severity) {
  debug("Commit message: ", commitMessage)
  return lint(commitMessage, rules).then((report) => {
    if (!report.valid) {
      let failureMessage = `There is a problem with the commit message\n> ${commitMessage}`
      report.errors.forEach((error) => {
        failureMessage = `${failureMessage}\n- ${error.message}`
      })
      reportFn(severity, failureMessage)
    }
  })
}

async function messageFailed(commitMessage, rules) {
  debug("Commit message: ", commitMessage)
  return lint(commitMessage, rules).then((report) => {
    if (!report.valid) {
      let failureMessage = `There is a problem with the commit message\n> ${commitMessage}`
      report.errors.forEach((error) => {
        failureMessage = `${failureMessage}\n- ${error.message}`
      })
      return true
    } else {
      return false
    }
  })
}

function isTrue(currentValue) {
  return currentValue === true
}

async function evalMessages(commits, rules, severity) {
  const tab: boolean[] = []
  for (const commit of commits) {
    const apromise = messageFailed(commit.message, rules)
    await apromise.then((res) => {
      tab.push(res)
    })
  }
  if (tab.every(isTrue)) {
    const failureMessage =
      "At least one commit message should be good. " +
      "The commit messages have to be squashed for the checks to only be performed on the MR title"
    reportFn(severity, failureMessage)
  }
}

function reportFn(severity, failureMessage) {
  switch (severity) {
    case "fail":
      fail(failureMessage)
      break
    case "warn":
      warn(failureMessage)
      break
    case "message":
      message(failureMessage)
      break
    case "disable":
    default:
      break
  }
}
